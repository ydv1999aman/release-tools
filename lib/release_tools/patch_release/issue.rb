# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class Issue < ReleaseTools::Issue
      BLOG_MR_STRING_IN_DESCRIPTION = 'BLOG_POST_MR'

      def title
        "Release #{version.to_ce}"
      end

      def labels
        'Monthly Release'
      end

      def project
        ReleaseTools::Project::Release::Tasks
      end

      def monthly_issue
        @monthly_issue ||= ReleaseTools::MonthlyIssue.new(version: version)
      end

      def link!
        return if version.monthly?

        ReleaseTools::GitlabClient.link_issues(self, monthly_issue)
      end

      def assignees
        ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      def blog_post_merge_request
        @blog_post_merge_request ||= PatchRelease::BlogMergeRequest.new(
          project: ReleaseTools::Project::WWWGitlabCom,
          version: version
        )
      end

      def add_blog_mr_to_description(blog_mr_url)
        Retriable.with_context(:api) do
          current_description = remote_issuable.description

          GitlabClient.edit_issue(
            project.path,
            iid,
            description: current_description.sub(BLOG_MR_STRING_IN_DESCRIPTION, blog_mr_url)
          )
        end
      end

      protected

      def template_path
        if version.rc?
          File.expand_path('../../../templates/release_candidate.md.erb', __dir__)
        else
          File.expand_path('../../../templates/patch.md.erb', __dir__)
        end
      end
    end
  end
end
