# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      # Perform production checks after gprd delay time has completed. Post a Slack
      # message and raise an error if production checks fail.
      class GprdDeploymentChecks
        include ::SemanticLogger::Loggable

        UnsafeProductionError = Class.new(StandardError)

        def initialize
          @deploy_version = ENV.fetch('DEPLOY_VERSION')
          @pipeline_url = ENV.fetch('CI_PIPELINE_URL')
        end

        def execute
          return if production_status.fine?

          send_slack_message

          logger.warn('Production checks have failed', failed_checks: production_status.failed_checks.collect(&:to_s))
          raise UnsafeProductionError, 'production checks have failed'
        end

        private

        attr_reader :deploy_version, :pipeline_url

        def production_status
          @production_status ||= ReleaseTools::Promotion::ProductionStatus.new(:canary_up)
        end

        def send_slack_message
          Retriable.retriable do
            ReleaseTools::Slack::ChatopsNotification.fire_hook(
              text: 'GPRD deployment blocked',
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              blocks: foreword_slack_blocks + production_status.to_slack_blocks
            )
          end
        end

        def foreword_slack_blocks
          text = StringIO.new
          text.puts(":red_circle: #{release_managers_mention} GPRD deployment blocked")
          text.puts
          text.puts("Package version: `#{deploy_version}`")
          text.puts("Deployment: <#{pipeline_url}|pipeline>")

          blocks = ::Slack::BlockKit.blocks
          blocks.section { |block| block.mrkdwn(text: text.string) }
          blocks.as_json
        end

        def release_managers_mention
          "<!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}>"
        end
      end
    end
  end
end
