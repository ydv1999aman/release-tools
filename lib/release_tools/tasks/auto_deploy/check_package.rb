# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class CheckPackage
        include ::SemanticLogger::Loggable

        PackageCommitOlderThanLatestError = Class.new(StandardError)

        def initialize
          @environment = ENV.fetch('DEPLOY_ENVIRONMENT')
          @deploy_version = ENV.fetch('DEPLOY_VERSION')
        end

        def execute
          comparison.map_components do |project_compare, source_sha, target_sha, project|
            raise_if_commit_older_than_latest(project_compare, source_sha, target_sha, project)
          end
        end

        private

        attr_reader :deploy_version, :environment

        # If there are no commits in diff, and we were not comparing the same SHAs, it means
        # that the commit we're deploying is older than the commit on the environment.
        def raise_if_commit_older_than_latest(compare, source_sha, target_sha, project)
          return if compare.commits.any? || compare.compare_same_ref

          log_msg(source_sha, target_sha, project)

          return if SharedStatus.dry_run? || !Feature.enabled?(:stop_deployment_if_old_package)

          raise PackageCommitOlderThanLatestError, error_message(source_sha, target_sha, project)
        end

        def comparison
          return @comparison if @comparison

          compare_service = Metadata::CompareService.new(
            source: ProductVersion.from_auto_deploy(deploy_version),
            environment: environment
          )

          @comparison = compare_service.with_latest_successful_deployment
        end

        def log_msg(source_sha, target_sha, project)
          logger.warn(
            'Package might be older than latest on environment',
            environment: environment,
            deploy_version: deploy_version,
            version_on_env: comparison.target.auto_deploy_package,
            source_sha: source_sha,
            sha_on_env: target_sha,
            project: project.security_path,
            feature_flag: Feature.enabled?(:stop_deployment_if_old_package),
            dry_run: SharedStatus.dry_run?
          )
        end

        def error_message(source_sha, target_sha, project)
          <<~MESSAGE
            This package (#{deploy_version}) uses commit '#{source_sha}' for '#{project.security_path}',
            and the latest commit deployed on #{environment} is '#{target_sha}'. The comparison between
            these two commits returns no commits, which could mean the commit being deployed is older
            than the one already on #{environment}. Stopping deployment!
          MESSAGE
        end
      end
    end
  end
end
