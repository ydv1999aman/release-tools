# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ProductionCheck
      # Perform a production health check during an ongoing deployment step.
      #
      # Results are posted to a Slack thread.
      #
      # See `DeploymentCheckReport`
      class DeploymentStep
        include ::SemanticLogger::Loggable

        UnsafeProductionError = Class.new(StandardError)

        def initialize
          @current_step = ENV.fetch('DEPLOYMENT_STEP')
          @deploy_version = ENV.fetch('DEPLOY_VERSION')
        end

        def execute
          foreword = Promotion::DeploymentCheckForeword.new(omnibus_package_version, @current_step, deployment_job_url)

          # FIXME (rspeicher): Can the report get the step from the foreword?
          # FIXME (rspeicher): Why doesn't the report get the status itself?
          Promotion::DeploymentCheckReport.new(@current_step, foreword, status).execute
        end

        private

        def omnibus_package_version
          @omnibus_package_version ||= ReleaseTools::Version.new(@deploy_version)
        end

        def deployment_job_url
          ENV['DEPLOYER_JOB_URL']
        end

        def status
          @status ||= Promotion::ProductionStatus.new
        end
      end
    end
  end
end
