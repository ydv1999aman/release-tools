# frozen_string_literal: true

require 'sentry-raven'

module ReleaseTools
  module Promotion
    class StatusNote
      attr_reader :status, :package_version, :override_reason

      def initialize(status:, package_version:, override_reason:)
        @status = status
        @package_version = package_version
        @ci_pipeline_url = ci_pipeline_url
        @override_reason = override_reason
      end

      def override_status?
        override_reason != 'false'
      end

      def ci_pipeline_url
        ENV.fetch('DEPLOYER_PIPELINE_URL', ENV['CI_PIPELINE_URL'])
      end

      def release_manager
        ops_user = ENV['RELEASE_MANAGER']
        return ':warning: Unknown release manager! Missing `RELEASE_MANAGER` variable' if ops_user.nil?

        user = ReleaseManagers::Definitions.new.find_user(ops_user, instance: :ops)

        return ":warning: Unknown release manager! OPS username #{ops_user}" if user.nil?

        "@#{user.production}"
      end

      def body
        ERB
          .new(template, trim_mode: '-') # Omit blank lines when using `<% -%>`
          .result(binding)
      end

      private

      def template
        template_path = File.expand_path('../../../templates/status_note.md.erb', __dir__)

        File.read(template_path)
      end
    end
  end
end
