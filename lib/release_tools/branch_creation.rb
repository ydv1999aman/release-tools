# frozen_string_literal: true

module ReleaseTools
  module BranchCreation
    Result = Struct.new(:project, :branch, :response)

    def gitlab_client
      ReleaseTools::GitlabClient
    end

    def create_branch_from_ref(project, branch_name, ref)
      logger&.info('Creating branch', name: branch_name, from: ref, project: project)

      return if ReleaseTools::SharedStatus.dry_run?

      branch =
        Retriable.with_context(:api) do
          gitlab_client.find_or_create_branch(branch_name, ref, project)
        end

      Result.new(
        project,
        branch_name,
        branch
      )
    end
  end
end
