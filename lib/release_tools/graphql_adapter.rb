# frozen_string_literal: true

require 'graphql/client/http'

module ReleaseTools
  class GraphqlAdapter
    include ::SemanticLogger::Loggable

    GraphqlError = Class.new(StandardError)

    GRAPHQL_ERROR_MESSAGE = 'A GraphQL query returned some errors'
    USER_AGENT = "GitLab Release Tools"

    def initialize(token:, endpoint:)
      @token = token
      @endpoint = endpoint
    end

    def query(graphql_query, variables = {})
      response = client.query(graphql_query, variables: variables, context: { token: @token })

      raise_on_error!(response, graphql_query)

      response.data
    end

    delegate :parse, to: :client

    private

    def raise_on_error!(response, graphql_query)
      return if response.errors.blank?

      logger.warn(
        GRAPHQL_ERROR_MESSAGE,
        query: graphql_query,
        errors: response.errors.messages
      )
      raise GraphqlError, "#{GRAPHQL_ERROR_MESSAGE}: #{response.errors.messages.to_json}"
    end

    def http_client
      GraphQL::Client::HTTP.new(@endpoint) do
        def headers(context) # rubocop:disable Lint/NestedMethodDefinition
          {
            'User-Agent' => USER_AGENT,
            'Content-type' => 'application/json',
            'PRIVATE-TOKEN' => context[:token]
          }
        end
      end
    end

    def schema
      @schema ||= GraphQL::Client.load_schema(http_client)
    end

    def client
      @client ||= GraphQL::Client.new(schema: schema, execute: http_client)
    end
  end
end
