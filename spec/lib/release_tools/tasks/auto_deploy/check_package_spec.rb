# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::AutoDeploy::CheckPackage do
  let(:deploy_version) { '14.3.202108261320-1c87faa073d.2cec58b7eb5' }
  let(:environment) { 'gstg' }
  let(:source) { ReleaseTools::ProductVersion.new('14.3.202108261320') }
  let(:compare_service) { instance_spy(ReleaseTools::Metadata::CompareService, with_latest_successful_deployment: comparison) }
  let(:comparison) { instance_spy(ReleaseTools::Metadata::Comparison, target: double(auto_deploy_package: '14.3.202108261520-1c87faa073d.2cec58b7eb5')) }
  let(:project) { class_spy(ReleaseTools::Project::GitlabEe, security_path: 'gitlab-org/security/gitlab') }

  let(:compare1) { build(:gitlab_response, commits: %w[sha1 sha2], compare_same_ref: false) }
  let(:compare2) { build(:gitlab_response, commits: [], compare_same_ref: true) }
  let(:compare3) { build(:gitlab_response, commits: [], compare_same_ref: false) }

  describe '#execute' do
    around do |test|
      ClimateControl.modify(DEPLOY_VERSION: deploy_version, DEPLOY_ENVIRONMENT: environment) do
        test.run
      end
    end

    before do
      allow(ReleaseTools::ProductVersion).to receive(:from_auto_deploy).with(deploy_version).and_return(source)

      allow(ReleaseTools::Metadata::CompareService)
        .to receive(:new)
        .with(source: source, environment: environment)
        .and_return(compare_service)

      allow(comparison)
        .to receive(:map_components)
        .and_yield(compare1, 'source_sha1', 'target_sha1', :project1)
        .and_yield(compare2, 'source_sha2', 'target_sha2', :project2)
        .and_yield(compare3, 'source_sha3', 'target_sha3', project)

      enable_feature(:stop_deployment_if_old_package)
    end

    it 'raises error if source is older than target' do
      without_dry_run do
        expect { subject.execute }.to raise_error(described_class::PackageCommitOlderThanLatestError, a_string_matching(deploy_version))
      end
    end

    it 'does not raise error in dry run mode' do
      expect { subject.execute }.not_to raise_error
    end

    context 'when feature flag disabled' do
      before do
        disable_feature(:stop_deployment_if_old_package)
      end

      it 'does not raise error' do
        without_dry_run do
          expect { subject.execute }.not_to raise_error
        end
      end
    end
  end
end
