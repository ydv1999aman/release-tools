# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Checks::ActiveIncidents do
  subject(:check) { described_class.new }

  describe '#name' do
    it 'includes blocking label references' do
      expect(check.name).to eq('active ~"severity::1" / ~"severity::2" incidents')
    end
  end

  describe '#fine?' do
    def stub_issues(*issues)
      allow(ReleaseTools::GitlabClient).to receive(:issues).and_return(issues)
    end

    it 'returns true with no incidents' do
      stub_issues

      expect(check).to be_fine
    end

    it 'returns false with an active severity::1 incident' do
      stub_issues(create(:issue, labels: %w[Incident::Active severity::1]))

      expect(check).not_to be_fine
    end

    it 'returns false with an active severity::2 incident' do
      stub_issues(create(:issue, labels: %w[Incident::Active severity::2]))

      expect(check).not_to be_fine
    end

    it 'returns true with an active severity::3 incident' do
      stub_issues(create(:issue, labels: %w[Incident::Active severity::3]))

      expect(check).to be_fine
    end

    it 'returns true with an active severity::4 incident' do
      stub_issues(create(:issue, labels: %w[Incident::Active severity::4]))

      expect(check).to be_fine
    end
  end
end
