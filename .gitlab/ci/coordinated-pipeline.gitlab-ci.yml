# Jobs associated to https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/154

.if-coordinated-pipeline: &if-coordinated-pipeline
  if: '$CI_COMMIT_TAG =~ /^\d+\.\d+\.\d{12}$/'

.if-auto-deploy-tag: &if-auto-deploy-tag
  if: '$AUTO_DEPLOY_TAG'

.track_deployment_started:
  extends: .with-bundle
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake 'metrics:deployment_started'

.notify_base:
  extends: .with-bundle
  variables:
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
  allow_failure: true
  script:
    - bundle exec rake 'auto_deploy:notify'

.notify_start:
  extends: .notify_base
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success

.notify_success:
  extends: .notify_base
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success

.notify_failure:
  extends: .notify_base
  rules:
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure

.qa_base:
  extends: .with-bundle
  allow_failure: true
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success

# Create package tags in order to initiate the building process.
#
# This job will only run on a tagged release-tools pipeline matching a specific
# format.
auto_deploy:start:
  extends: .with-bundle
  stage: coordinated:tag
  rules:
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake 'auto_deploy:tag'
  artifacts:
    reports:
      dotenv: deploy_vars.env

# Wait on an Omnibus pipeline to complete in order to ensure the package is
# built before attempting to deploy it.
#
# Runs under one of two conditions:
#
# 1. In a tagged release-tools pipeline after `auto_deploy:start`, delayed by 60
#    minutes since packages usually take at least this long to build.
# 2. In the presence of an `AUTO_DEPLOY_TAG` variable, populated when we want to
#    trigger a deploy directly from ChatOps. There is no delay in this case.
wait:omnibus:
  extends: .with-bundle
  stage: coordinated:build
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 60 minutes
  script:
    - bundle exec rake 'auto_deploy:wait:omnibus'

# Wait on a Helm chart pipeline to complete in order to ensure the image is
# built before attempting to deploy it.
#
# Runs under one of two conditions:
#
# 1. In a tagged release-tools pipeline after `auto_deploy:start`, delayed by 45
#    minutes since images usually take at least this long to build.
# 2. In the presence of an `AUTO_DEPLOY_TAG` variable, populated when we want to
#    trigger a deploy directly from ChatOps. There is no delay in this case.
wait:helm:
  extends: .with-bundle
  stage: coordinated:build
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 45 minutes
  script:
    - bundle exec rake 'auto_deploy:wait:helm'

# Registers the deployment start time and stores the value in `DEPLOY_START_TIME`.
#
# Runs after the auto_deploy packages have been built and before the staging
# deployment starts.
metrics:start_time:
  extends: .with-bundle
  stage: coordinated:metrics:prepare
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:helm
  script:
    - bundle exec rake 'auto_deploy:metrics:start_time'
  allow_failure: true
  artifacts:
    reports:
      dotenv: deploy_vars.env

# Send a slack notification about the start of the staging ref deployment.
#
# Runs after the initial metrics (metrics:start) have been collected.
notify_start:gstg-ref:
  extends: .notify_start
  stage: coordinated:deploy:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
  needs:
    - auto_deploy:start
    - metrics:start_time

# Trigger a downstream pipeline to deploy to staging ref
deploy:gstg-ref:
  stage: coordinated:deploy:staging-canary-and-ref
  allow_failure: true
  rules:
    - if: '$SKIP_REF_CNY_DEPLOYMENT'
      when: never
    - if: '$SKIP_GSTG_CNY_DEPLOYMENT'
      when: never
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:helm
    - metrics:start_time
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

# metrics:deployment_started:gstg-ref:
#   extends: .track_deployment_started
#   stage: coordinated:deploy:staging-canary-and-ref
#   variables:
#     DEPLOY_ENVIRONMENT: 'gstg-ref'
#   needs:
#     - auto_deploy:start # we need DEPLOY_VERSION env artifact
#     - metrics:start_time

# Sends a slack notification notifying a successful deployment for gstg-ref
notify_success:gstg-ref:
  extends: .notify_success
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
  needs:
    - auto_deploy:start
    - deploy:gstg-ref

# Sends a slack notification notifying a failed deployment for gstg-ref
notify_failure:gstg-ref:
  extends: .notify_failure
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
  needs:
    - auto_deploy:start
    - deploy:gstg-ref

# Send a slack notification about the start of the staging canary deployment.
#
# Runs after the initial metrics (metrics:start) have been collected.
notify_start:gstg-cny:
  extends: .notify_start
  stage: coordinated:deploy:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - metrics:start_time

# Trigger a downstream pipeline to deploy to staging canary
deploy:gstg-cny:
  stage: coordinated:deploy:staging-canary-and-ref
  rules:
    - if: '$SKIP_GSTG_CNY_DEPLOYMENT'
      when: never
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:helm
    - metrics:start_time
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

metrics:deployment_started:gstg-cny:
  extends: .track_deployment_started
  stage: coordinated:deploy:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - metrics:start_time

# Sends a slack notification notifying a successful deployment for gstg-cny
notify_success:gstg-cny:
  extends: .notify_success
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - deploy:gstg-cny

# Sends a slack notification notifying a failed deployment for gstg-cny
notify_failure:gstg-cny:
  extends: .notify_failure
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - deploy:gstg-cny

# Send a slack notification about the start of the staging deployment.
#
# Runs after the notification of the staging-canary deployment has been sent.
notify_start:gstg:
  extends: .notify_start
  stage: coordinated:deploy:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - notify_success:gstg-cny

# Trigger a downstream pipeline to deploy to staging
deploy:gstg:
  stage: coordinated:deploy:staging
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
    POSTDEPLOY_MIGRATIONS: 'false'
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:helm
    - metrics:start_time
    - deploy:gstg-cny
    - notify_success:gstg-cny
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

metrics:deployment_started:gstg:
  extends: .track_deployment_started
  stage: coordinated:deploy:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - notify_success:gstg-cny

# Trigger a downstream pipeline to do postdeploy migrations
postdeploy-migrations:gstg:
  stage: coordinated:postdeploy-migrations:staging
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
    POSTDEPLOY_MIGRATIONS: 'true'
  needs:
    - auto_deploy:start
    - deploy:gstg
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

qa:issue:gstg:
  extends: .qa_base
  stage: coordinated:qa:staging
  variables:
    DEPLOY_VERSION: $DEPLOY_VERSION
  needs:
    - auto_deploy:start
    - postdeploy-migrations:gstg
  script:
    - bundle exec rake 'quality:find_issue'
  artifacts:
    reports:
      dotenv: deploy_vars.env

# Triggers a Full Quality pipeline on quality/staging
qa:full:gstg:
  stage: coordinated:qa:staging
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    DEPLOY_VERSION: $DEPLOY_VERSION
    GITLAB_QA_ISSUE_URL: $GITLAB_QA_ISSUE_URL
    FULL_ONLY: 'true'
  needs:
    - auto_deploy:start
    - qa:issue:gstg
  trigger:
    project: gitlab-org/quality/staging

# Triggers a Smoke Quality pipeline on quality/staging
qa:smoke:gstg:
  stage: coordinated:qa:staging
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    DEPLOY_VERSION: $DEPLOY_VERSION
    GITLAB_QA_ISSUE_URL: $GITLAB_QA_ISSUE_URL
    SMOKE_ONLY: 'true'
  needs:
    - auto_deploy:start
    - qa:issue:gstg
  trigger:
    project: gitlab-org/quality/staging
    strategy: depend

# Sends a slack notification notifying a successful deployment for gstg
notify_success:gstg:
  extends: .notify_success
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - qa:smoke:gstg

# Sends a slack notification notifying a failed deployment for gstg
notify_failure:gstg:
  extends: .notify_failure
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - deploy:gstg

notify_failure:gstg:postdeploy-migrations:
  extends: .notify_failure
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - postdeploy-migrations:gstg
  script:
    - bundle exec rake 'post_deploy_migrations:notify'

# Sends a slack notification notifying a failed QA for gstg
notify_failure:gstg:qa:
  extends: .notify_failure
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    DEPLOY_VERSION: $DEPLOY_VERSION
  needs:
    - auto_deploy:start
    - qa:smoke:gstg
  script:
    - bundle exec rake 'quality:notify'

# Send a slack notification about the start of the canary deployment
#
# Runs after the notification of the staging deployment has been sent.
notify_start:gprd-cny:
  extends: .notify_start
  stage: coordinated:deploy:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - notify_success:gstg

# Trigger a downstream pipeline to deploy to canary
deploy:gprd-cny:
  stage: coordinated:deploy:canary
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:helm
    - metrics:start_time
    - deploy:gstg
    - notify_success:gstg
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

metrics:deployment_started:gprd-cny:
  extends: .track_deployment_started
  stage: coordinated:deploy:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - notify_success:gstg

# Triggers a Full Quality pipeline on quality/canary
qa:full:gprd-cny:
  stage: coordinated:qa:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    FULL_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gprd-cny
  trigger:
    project: gitlab-org/quality/canary

# Triggers a Smoke Quality pipeline on quality/canary
qa:smoke:gprd-cny:
  stage: coordinated:qa:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SMOKE_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gprd-cny
  trigger:
    project: gitlab-org/quality/canary
    strategy: depend

# Triggers an Smoke Quality pipeline on quality/production
qa:smoke-main:gprd-cny:
  stage: coordinated:qa:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SMOKE_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gprd-cny
  trigger:
    project: gitlab-org/quality/production
    strategy: depend

# Sends a slack notification notifying a successful deployment for gprd-cny
notify_success:gprd-cny:
  extends: .notify_success
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny

# Sends a slack notification notifying a failed deployment for gprd-cny
notify_failure:gprd-cny:
  extends: .notify_failure
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - deploy:gprd-cny

# Sends a slack notification notifying a failed QA for gprd-cny
notify_failure:gprd-cny:qa:
  extends: .notify_failure
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
  needs:
    - auto_deploy:start
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny
  script:
    - bundle exec rake 'quality:notify'

# Triggers production checks before authorizing a deployment to production
#
# Runs after 60 minutes and if a deployment to canary
# has been completed.
baking_time:
  tags:
    # Internal prometheus is only available from specific tagged runners
    - release
  extends: .with-bundle
  stage: coordinated:promote:production
  rules:
    - <<: *if-auto-deploy-tag
      when: delayed
      start_in: 60 minutes
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 60 minutes
  needs:
    - notify_success:gprd-cny
  script:
    - export LOG_LEVEL=trace  # More verbose logging while we debug
    - unset ELASTIC_URL       # Don't send these verbose logs to Elastic
    - bundle exec rake 'auto_deploy:baking_time'

# Triggers a production check that validates if a deploy to production
# can start
promote:gprd:
  extends: .with-bundle
  tags:
    - release
  stage: coordinated:promote:production
  rules:
    - <<: *if-auto-deploy-tag
      when: manual
    - <<: *if-coordinated-pipeline
      when: manual
  variables:
    RELEASE_MANAGER: '$GITLAB_USER_LOGIN'
  script:
    - bundle exec rake 'auto_deploy:check_production'

metrics:deployment_started:gprd:
  extends: .track_deployment_started
  stage: coordinated:deploy:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - promote:gprd

# Send a slack notification about the start of the production deployment
#
# Runs after the deployment checks (promote:gprd)
# have been cleared
notify_start:gprd:
  extends: .notify_start
  stage: coordinated:deploy:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - promote:gprd

# Triggers a downstream pipeline to deploy to production
#
# Runs after the deployment checks (promote:gprd)
# have been cleared
deploy:gprd:
  stage: coordinated:deploy:production
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
    POSTDEPLOY_MIGRATIONS: 'false'
  needs:
    - auto_deploy:start
    - promote:gprd
    - metrics:start_time
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

# Triggers a downstream pipeline to do postdeploy migrations
postdeploy-migrations:gprd:
 stage: coordinated:postdeploy-migrations:production
 rules:
   - <<: *if-auto-deploy-tag
   - <<: *if-coordinated-pipeline
 variables:
   DEPLOY_ENVIRONMENT: 'gprd'
   TRIGGER_REF: 'master'
   DEPLOY_USER: 'deployer'
   DEPLOY_VERSION: $DEPLOY_VERSION
   SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
   SKIP_PIPELINE_NOTIFY: 'true'
   POSTDEPLOY_MIGRATIONS: 'true'
 needs:
   - auto_deploy:start
   - deploy:gprd
 trigger:
   project: gitlab-com/gl-infra/deployer
   strategy: depend

# Sends a slack notification notifying a successful deployment for gprd
notify_success:gprd:
  extends: .notify_success
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - postdeploy-migrations:gprd

# Sends a slack notification notifying a failed deployment for gprd
notify_failure:gprd:
  extends: .notify_failure
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - deploy:gprd

notify_failure:gprd:postdeploy-migrations:
  extends: .notify_failure
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - postdeploy-migrations:gprd
  script:
    - bundle exec rake 'post_deploy_migrations:notify'

# Registers the deployment end time and calculates the deployment duration
# based on `DEPLOY_START_TIME` value. Pushes the information to Prometheus PushGateway
#
# Runs after the deployment to production finishes.
metrics:end_time:
  tags:
    # Internal prometheus is only available from specific tagged runners
    - release
  extends: .with-bundle
  stage: coordinated:finish
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_START_TIME: $DEPLOY_START_TIME
    DEPLOY_VERSION: $DEPLOY_VERSION
  allow_failure: true
  needs:
    - auto_deploy:start
    - metrics:start_time
    - notify_success:gprd
  script:
    - bundle exec rake 'auto_deploy:metrics:end_time'
