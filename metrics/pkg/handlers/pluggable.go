package handlers

import "github.com/gorilla/mux"

type Pluggable interface {
	PlugRoutes(r *mux.Router)
}
